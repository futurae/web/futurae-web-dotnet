## Overview

**futurae-web-dotnet** - Provides the Futurae Web .NET helpers to be integrated in your .NET web
application.

These helpers allow developers to integrate Futurae's Web authentication suite into their web apps,
without the need to implement the Futurae Auth API.


## Example
Fill in your Futurae Web Widget credentials in `Example/App.config` and run the project in Visual Studio.

You can then visit `http://localhost:8080/?username=ausername` and check the Futurae Web Widget integration.


## Documentation

Documentation on using the Futurae Web Widget can be found [here](https://futurae.com/docs/guide/futurae-web/).