using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Web;

namespace Example
{
    class Program
    {
        private static string wid;
        private static string wkey;
        private static string skey;
        private static string host;

        static void Main(string[] args)
        {
            ParseConfiguration();
            WebServer server = new WebServer(SendResponse, "http://localhost:8080/");
            server.Run();
            PrintStartupMessaging();
            Console.ReadKey();
            server.Stop();
        }

        private static void PrintStartupMessaging()
        {
            Console.WriteLine("Open the following URL in your browser:");
            Console.WriteLine("http://localhost:8080/?username=test@example.com");
        }

        private static void ParseConfiguration()
        {
            wid = ConfigurationManager.AppSettings["wid"];
            wkey = ConfigurationManager.AppSettings["wkey"];
            skey = ConfigurationManager.AppSettings["skey"];
            host = ConfigurationManager.AppSettings["host"];
        }

        public static string SendResponse(HttpListenerRequest request)
        {
            if (String.Compare(request.HttpMethod, "POST", true) == 0)
            {
                return doPost(request);
            }
            return doGet(request);
        }

        private static string doPost(HttpListenerRequest request)
        {
            using (System.IO.Stream body = request.InputStream)
            {
                using (System.IO.StreamReader reader = new System.IO.StreamReader(body, request.ContentEncoding))
                {
                    String bodyStream = reader.ReadToEnd();
                    var form = bodyStream.Split('=');
                    var sig_response = System.Net.WebUtility.UrlDecode(form[1]);
                    String responseUser = Futurae.Web.VerifyResponse(wid, wkey, skey, sig_response);
                    if (String.IsNullOrEmpty(responseUser))
                    {
                        return "Authentication failed.";
                    }
                    else
                    {
                        return String.Format("{0} authenticated successfully!", responseUser);
                    }
                }
            }
        }

        private static string doGet(HttpListenerRequest request)
        {
            String response = String.Empty;

            try
            {
                response = System.IO.File.ReadAllText(System.IO.Path.GetFileName(request.RawUrl));
            }
            catch (Exception e)
            {
                String username = request.QueryString.Get("username");
                if (String.IsNullOrEmpty(username))
                    return String.Format("Please specify a user to authenticate");

                var sig_request = Futurae.Web.SignRequest(wid, wkey, skey, username);
                response = String.Format(@"<html>
                  <head>
                    <title>Futurae Authentication</title>
                    <meta charset='utf-8'>
                    <meta name='viewport' content='width=device-width, initial-scale=1'>
                    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                  </head>
                  <body>
                    <h1>Futurae Authentication</h1>
                    <script src='/Futurae-Web-SDK-v1.js'></script>
                    <iframe id='futurae_widget' style='width: 800px; height: 501px;'
                            title='Two-Factor Authentication'
                            frameborder='0'
                            data-host='{0}'
                            data-sig-request='{1}'
                            data-lang='en'
                            allow='microphone'>
                    </iframe>
                  </body>
                </html>", host, sig_request);
            }

            return response;
        }
    }
}
