﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FuturaeWebTests
{
    [TestClass]
    public class FuturaeWebTests
    {
        const string wid = "FIDXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
        const string wkey = "futuraegeneratedsharedwebapplicationkey.";
        const string skey = "useselfgeneratedhostapplicationsecretkey";
        const string username = "testusername";

        private string valid_app_sig;
        private string invalid_app_sig;

        [TestInitialize()]
        public void SetUp()
        {
            string request_sig = Futurae.Web.SignRequest(wid, wkey, skey, username);
            string[] signatures = request_sig.Split(':');
            string futurae_sig = signatures[0];
            valid_app_sig = signatures[1];

            request_sig = Futurae.Web.SignRequest(wid, wkey, new string('i', 40), username);
            signatures = request_sig.Split(':');
            invalid_app_sig = signatures[1];
        }

        [TestMethod]
        public void TestSignRequestWhenValid()
        {
            string request_sig = Futurae.Web.SignRequest(wid, wkey, skey, username);
            Assert.IsNotNull(request_sig);
        }

        [TestMethod]
        public void TestSignRequestNoUsername()
        {
            string request_sig = Futurae.Web.SignRequest(wid, wkey, skey, "");
            Assert.AreEqual(request_sig, Futurae.Web.ERR_USERNAME);
        }

        [TestMethod]
        public void TestSignRequestInvalidUsername()
        {
            string request_sig = Futurae.Web.SignRequest(wid, wkey, skey, "in|valid");
            Assert.AreEqual(request_sig, Futurae.Web.ERR_USERNAME);
        }

        [TestMethod]
        public void TestSignRequestInvalidWID()
        {
            string request_sig = Futurae.Web.SignRequest("invalid", wkey, skey, username);
            Assert.AreEqual(request_sig, Futurae.Web.ERR_WID);
        }

        [TestMethod]
        public void TestSignRequestInvalidWKEY()
        {
            string request_sig = Futurae.Web.SignRequest(wid, "invalid", skey, username);
            Assert.AreEqual(request_sig, Futurae.Web.ERR_WKEY);
        }

        [TestMethod]
        public void TestSignRequestInvalidSKEY()
        {
            string request_sig = Futurae.Web.SignRequest(wid, wkey, "invalid", username);
            Assert.AreEqual(request_sig, Futurae.Web.ERR_SKEY);
        }

        [TestMethod]
        public void TestVerifyResponseValid()
        {
            string sig_response = TestHelpers.FutureResponse(wid, wkey, username) + ":" + valid_app_sig;
            string valid_user = Futurae.Web.VerifyResponse(wid, wkey, skey, sig_response);

            Assert.AreEqual(valid_user, username);
        }

        [TestMethod]
        public void TestVerifyResponseInvalidResponse()
        {
            string sig_response = TestHelpers.InvalidResponse() + ":" + valid_app_sig;
            string invalid_user = Futurae.Web.VerifyResponse(wid, wkey, skey, sig_response);

            Assert.IsNull(invalid_user);
        }

        [TestMethod]
        public void TestVerifyResponseExpired()
        {
            string sig_response = TestHelpers.ExpiredResponse(wid, wkey, username) + ":" + valid_app_sig;
            string expired_user = Futurae.Web.VerifyResponse(wid, wkey, skey, sig_response);

            Assert.IsNull(expired_user);
        }

        [TestMethod]
        public void TestVerifyResponseInvalidAppSignature()
        {
            string sig_response = TestHelpers.FutureResponse(wid, wkey, username) + ":" + invalid_app_sig;
            string invalid_user = Futurae.Web.VerifyResponse(wid, wkey, skey, sig_response);

            Assert.IsNull(invalid_user);
        }

        [TestMethod]
        public void TestVerifyResponseInvalidBadParamsResponse()
        {
            string sig_response = TestHelpers.BadParamsResponse(wid, wkey, username) + ":" + valid_app_sig;
            string invalid_user = Futurae.Web.VerifyResponse(wid, wkey, skey, sig_response);

            Assert.IsNull(invalid_user);
        }

        [TestMethod]
        public void TestVerifyResponseBadParamsApp()
        {
            string bad_app_params = TestHelpers.BadParamsApp(wid, wkey, username);
            string sig_response = TestHelpers.FutureResponse(wid, wkey, username) + ":" + bad_app_params;
            string invalid_user = Futurae.Web.VerifyResponse(wid, wkey, skey, sig_response);

            Assert.IsNull(invalid_user);
        }

        [TestMethod]
        public void TestVerifyResponseInvalidWID()
        {
            string wrong_wid = "FIXXXXXXXXXXXXXXXXXY";
            string sig_response = TestHelpers.FutureResponse(wid, wkey, username) + ":" + valid_app_sig;
            string invalid_user = Futurae.Web.VerifyResponse(wrong_wid, wkey, skey, sig_response);

            Assert.IsNull(invalid_user);
        }
    }
}
