﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace FuturaeWebTests
{
    class TestHelpers
    {
        private static Encoding _encoding = new UTF8Encoding(false, true);

        public static string BadParamsApp(string wid, string skey, string username)
        {
            string[] vals = { wid, username, ExpiredExpiry(), "bad_param" };
            string cookie = Cookie("APP", vals);
            string ctx_hex = Hmac_sha256(skey, cookie);

            return cookie + "|" + ctx_hex;
        }

        public static string BadParamsResponse(string wid, string wkey, string username)
        {
            string[] vals = { wid, username, ExpiredExpiry(), "bad_param" };
            string cookie = Cookie("AUTH", vals);
            string ctx_hex = Hmac_sha256(wkey, cookie);

            return cookie + "|" + ctx_hex;
        }

        public static string ExpiredResponse(string wid, string wkey, string username)
        {
            string[] vals = { wid, username, ExpiredExpiry() };
            string cookie = Cookie("AUTH", vals);
            string ctx_hex = Hmac_sha256(wkey, cookie);

            return cookie + "|" + ctx_hex;
        }

        public static string FutureResponse(string wid, string wkey, string username)
        {
            string[] vals = { wid, username, FutureExpiry() };
            string cookie = Cookie("AUTH", vals);
            string ctx_hex = Hmac_sha256(wkey, cookie);

            return cookie + "|" + ctx_hex;
        }

        public static string FutureEnrollResponse(string wid, string wkey, string username)
        {
            string[] vals = { wid, username, FutureExpiry() };
            string cookie = Cookie("ENROLL", vals);
            string ctx_hex = Hmac_sha256(wkey, cookie);

            return cookie + "|" + ctx_hex;
        }

        public static string InvalidResponse()
        {
            return "AUTH|INVALID|SIG";
        }

        private static string Cookie(string prefix, string[] vals)
        {
            string wid = vals[0];
            string username = vals[1];
            string expire = vals[2];
            string val = wid + "|" + username + "|" + expire;

            return prefix + "|" + Encode64(val);
        }

        private static string Encode64(string plaintext)
        {
            byte[] plaintext_bytes = _encoding.GetBytes(plaintext);
            return System.Convert.ToBase64String(plaintext_bytes);
        }

        private static string FutureExpiry()
        {
            DateTime now = DateTime.UtcNow;
            Int64 ts = (Int64)(now - new DateTime(1970, 1, 1)).TotalSeconds + 600;
            return ts.ToString();
        }

        private static string ExpiredExpiry()
        {
            DateTime now = DateTime.UtcNow;
            Int64 ts = (Int64)(now - new DateTime(1970, 1, 1)).TotalSeconds - 600;
            return ts.ToString();
        }

        private static string Hmac_sha256(string key, string data)
        {
            byte[] keyBytes = _encoding.GetBytes(key);
            HMACSHA256 hmac = new HMACSHA256(keyBytes);

            byte[] dataBytes = _encoding.GetBytes(data);
            hmac.ComputeHash(dataBytes);

            string hex = BitConverter.ToString(hmac.Hash);
            return hex.Replace("-", "").ToLower();
        }
    }
}
