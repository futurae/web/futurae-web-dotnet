﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace Futurae
{
    public static class Web
    {
        public const string FUTURAE_PREFIX = "REQ";
        public const string APP_PREFIX = "APP";
        public const string AUTH_PREFIX = "AUTH";

        public const int FUTURAE_EXPIRY = 600;
        public const int APP_EXPIRY = 3600;

        public const int WID_LEN = 36;
        public const int WKEY_LEN = 40;
        public const int SKEY_LEN = 40;

        public const string ERR_USERNAME = "ERR|The username passed to sign_request() is invalid.";
        public const string ERR_WID = "ERR|The Futurae Web ID passed to sign_request() is invalid.";
        public const string ERR_WKEY = "ERR|The Futurae Web Key passed to sign_request() is invalid.";
        public const string ERR_SKEY = "ERR|The application Secret Key passed to sign_request() must be at least 40 characters.";

        private static Encoding _encoding = new UTF8Encoding(false, true);

        public static string SignRequest(string wid, string wkey, string skey, string username)
        {
            if (string.IsNullOrEmpty(username) || username.Contains("|"))
            {
                return ERR_USERNAME;
            }

            if (string.IsNullOrEmpty(wid) || wid.Length != WID_LEN)
            {
                return ERR_WID;
            }

            if (string.IsNullOrEmpty(wkey) || wkey.Length < WKEY_LEN)
            {
                return ERR_WKEY;
            }

            if (string.IsNullOrEmpty(skey) || skey.Length < SKEY_LEN)
            {
                return ERR_SKEY;
            }

            string[] vals = { wid, username };

            string sig = SignVals(wkey, vals, FUTURAE_PREFIX, FUTURAE_EXPIRY);
            string app_sig = SignVals(skey, vals, APP_PREFIX, APP_EXPIRY);


            return sig + ":" + app_sig;
        }

        public static string VerifyResponse(string wid, string wkey, string skey, string sig_response)
        {
            string[] vals = sig_response.Split(':');
            string auth_sig = vals[0];
            string app_sig = vals[1];

            string auth_user = ParseVals(wkey, auth_sig, AUTH_PREFIX, wid);
            string app_user = ParseVals(skey, app_sig, APP_PREFIX, wid);

            if (string.IsNullOrEmpty(auth_user) || !auth_user.Equals(app_user))
            {
                return null;
            }

            return auth_user;
        }

        private static string Hmac_sha256(string key, string data)
        {
            byte[] keyBytes = _encoding.GetBytes(key);
            HMACSHA256 hmac = new HMACSHA256(keyBytes);

            byte[] dataBytes = _encoding.GetBytes(data);
            hmac.ComputeHash(dataBytes);

            string hex = BitConverter.ToString(hmac.Hash);
            return hex.Replace("-", "").ToLower();
        }

        private static string SignVals(string key, string[] vals, string prefix, Int64 expire)
        {
            DateTime now = DateTime.UtcNow;
            Int64 ts = (Int64)(now - new DateTime(1970, 1, 1)).TotalSeconds;
            expire = ts + expire;

            string wid = vals[0];
            string username = vals[1];

            string val = wid + "|" + username + "|" + expire.ToString();
            string cookie = prefix + "|" + Encode64(val);

            string sig = Hmac_sha256(key, cookie);
            return cookie + "|" + sig;
        }

        private static string ParseVals(string key, string val, string prefix, string wid)
        {
            DateTime now = DateTime.UtcNow;
            Int64 ts = (int)(now - new DateTime(1970, 1, 1)).TotalSeconds;

            string[] parts = val.Split('|');
            if (parts.Length != 3)
            {
                return null;
            }

            string u_prefix = parts[0];
            string u_b64 = parts[1];
            string u_sig = parts[2];

            string sig = Hmac_sha256(key, u_prefix + "|" + u_b64);
            if (Hmac_sha256(key, sig) != Hmac_sha256(key, u_sig))
            {
                return null;
            }

            if (u_prefix != prefix)
            {
                return null;
            }

            string cookie = Decode64(u_b64);
            string[] cookie_parts = cookie.Split('|');
            if (cookie_parts.Length != 3)
            {
                return null;
            }

            string u_wid = cookie_parts[0];
            string username = cookie_parts[1];
            string expire = cookie_parts[2];

            if (u_wid != wid)
            {
                return null;
            }

            int expire_ts = Convert.ToInt32(expire);
            if (ts >= expire_ts)
            {
                return null;
            }

            return username;
        }

        private static string Encode64(string plaintext)
        {
            byte[] plaintext_bytes = _encoding.GetBytes(plaintext);
            return System.Convert.ToBase64String(plaintext_bytes);
        }

        private static string Decode64(string encoded)
        {
            byte[] plaintext_bytes = System.Convert.FromBase64String(encoded);
            return _encoding.GetString(plaintext_bytes);
        }
    }
}
